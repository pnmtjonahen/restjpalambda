package nl.ordina.restjpa;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestjpaApplication.class, args);
    }

    @PersistenceContext
    private EntityManager entityManager;

    public void code1(Date vanafBoekeingsDatum, Date totEnMetBoekingsDatum) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Boeking> cq = cb.createQuery(Boeking.class);
        final Root<Boeking> boekingRoot = cq.from(Boeking.class);
        cq.select(boekingRoot);

        Predicate pr = cb.between(boekingRoot.get(Boeking_.boekingDatum),
                vanafBoekeingsDatum,
                totEnMetBoekingsDatum);
        cq.where(pr);

    }

    public void code2(String veldNaam, String veldWaarde) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Boeking> cq = cb.createQuery(Boeking.class);
        final Root<Boeking> boekingRoot = cq.from(Boeking.class);
        cq.select(boekingRoot);

        Predicate pr = null;

        switch (veldNaam) {
            case "boekingsDatum":
                pr = cb.between(boekingRoot.get(Boeking_.boekingDatum),
                        vanafDatum(veldWaarde),
                        totEnMetDatum(veldWaarde));
                break;
            case "tegenRekening":
                pr = cb.equal(boekingRoot.get(Boeking_.tegenrekening), 
                        veldWaarde);
                break;
        }
        cq.where(pr);

    }

    public void code3(List<ApiVeld> apiVelden) {
        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Boeking> cq = cb.createQuery(Boeking.class);
        final Root<Boeking> boekingRoot = cq.from(Boeking.class);
        cq.select(boekingRoot);

        apiVelden.stream().map(apiVeld -> {
            switch (apiVeld.naam) {
                case "boekingsDatum":
                    return cb.between(boekingRoot.get(Boeking_.boekingDatum),
                            vanafDatum(apiVeld.waarde),
                            totEnMetDatum(apiVeld.waarde));
                case "tegenRekening":
                    return cb.equal(boekingRoot.get(Boeking_.tegenrekening), 
                            apiVeld.waarde);
            }
            return null;
        }).reduce((a, b) -> cb.and(a, b))
                .ifPresent(pr -> cq.where(pr));
    }

    @FunctionalInterface
    public interface PredicateSupplier {
        Predicate supply(CriteriaBuilder cb, Root<Boeking> root, String value);
    }

    public void code4(List<ApiVeld> apiVelden) {
        Map<String, PredicateSupplier> map = new TreeMap<>();

        map.put("boekingsDatum", new PredicateSupplier() {
            public Predicate supply(CriteriaBuilder cb, Root<Boeking> r, String v) {
                return cb.between(r.get(Boeking_.boekingDatum),
                        vanafDatum(v),
                        totEnMetDatum(v));

            }
        });

        map.put("tegenRekening",
                (cb, r, v) -> cb.equal(r.get(Boeking_.tegenrekening), v));

        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Boeking> cq = cb.createQuery(Boeking.class);
        final Root<Boeking> boekingRoot = cq.from(Boeking.class);
        cq.select(boekingRoot);

        apiVelden.stream()
                .map(apiVeld -> map.get(apiVeld.naam).supply(cb, boekingRoot, 
                        apiVeld.waarde))
                .reduce((a, b) -> cb.and(a, b))
                .ifPresent(pr -> cq.where(pr));

    }

    public enum PredicateField {
        TEGENREKENING((cb, r, v) -> cb.equal(r.get(Boeking_.tegenrekening), v)),
        BOEKINGSDATUM((cb, r, v) -> cb.between(r.get(Boeking_.boekingDatum),
                vanafDatum(v),
                totEnMetDatum(v)));

        private final PredicateSupplier ps;

        private PredicateField(PredicateSupplier ps) {
            this.ps = ps;
        }

        public Predicate predicate(CriteriaBuilder cb, Root<Boeking> root, String value) {
            return ps.supply(cb, root, value);
        }
    }

    public void code5(List<ApiVeld> apiVelden) {

        final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Boeking> cq = cb.createQuery(Boeking.class);
        final Root<Boeking> boekingRoot = cq.from(Boeking.class);
        cq.select(boekingRoot);

        apiVelden.stream()
                .map(apiVeld -> PredicateField.valueOf(apiVeld.naam.toUpperCase())
                .predicate(cb, boekingRoot, apiVeld.waarde))
                .reduce((a, b) -> cb.and(a, b))
                .ifPresent(pr -> cq.where(pr));

    }

    public class ApiVeld {

        String naam;
        String waarde;
    }

    private static Date vanafDatum(String v) {
        return null;
    }

    private static Date totEnMetDatum(String v) {
        return null;
    }

}

@Entity
class Boeking {

    @Id
    private Long id;
    private Date boekingDatum;
    private String tegenrekening;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBoekingDatum() {
        return boekingDatum;
    }

    public void setBoekingDatum(Date boekingDatum) {
        this.boekingDatum = boekingDatum;
    }

    public String getTegenrekening() {
        return tegenrekening;
    }

    public void setTegenrekening(String tegenrekening) {
        this.tegenrekening = tegenrekening;
    }

    
}
